# Relative Path Overwrite demo

RPO ("***Relative Path Overwrite***") is an elaborate attack technique that takes advantage of relative links to overwrite its target.

As described in the various RFCs ([RFC-1738 - Uniform Resource Locators (URL)](https://tools.ietf.org/html/rfc1738), [RFC-1808 - Relative Uniform Resource Locators (URL)](https://tools.ietf.org/html/rfc1808), [RFC-3986 - Uniform Resource Identifier (URI): Generic Syntax](https://tools.ietf.org/html/rfc3986)):

 - A URL is a specific scheme of *Uniform Resource Identifier* (URI) designed to create references to web resources;
 - Each URI conforms to a generic syntax (defined as `scheme://user:password@host:port/path?query#fragment` in RFC 3986) which consists of a hierarchical sequence of segments and subsegments referred to as the scheme (e.g., `scheme://`), authority (e.g., `user:password@host:port`), path (e.g., `/path`), query (e.g., `?query`) and fragment (e.g., #fragment);
 - URI segments are delimited by characters in a "reserved" set. Thus, segment-wide data characters belong to this reserved set must be percent-encoded to prevent conflicts with a reserved character's purpose as a delimiter. In this way, a URI that differ in the replacement of a reserved character with its corresponding **percent-encoded octet shouldn't be equivalent** and **should change how the URI is interpreted by the application** (backend and frontend).

An absolute URL is basically the "full URL" for a destination address (e.g., `https://www.bmoine.fr/assets/js/core.js?v=deadbeef#sha384-bOt9mrX4bzGOgv1vRvIwO4xe0VxLbHznZCMN9YnlBhsfW2149mKXNdhNQXXwYTno`) including the scheme (e.g., `https:`), the authority (e.g., `www.bmoine.fr`), the path (e.g., `/assets/js/core.js`) and enventually, the query (e.g., `?v=deadbeef`) and fragment part (e.g., `#sha384-bOt9mrX4bzGOgv1vRvIwO4xe0VxLbHznZCMN9YnlBhsfW2149mKXNdhNQXXwYTno`).

Whereas a relative URL doesn’t specify the scheme nor the authority and instead refers to a resource by describing the difference within a hierarchical namespace between the existing location and the target URL (e.g., `assets/js/core.js?v=deadbeef#sha384-bOt9mrX4bzGOgv1vRvIwO4xe0VxLbHznZCMN9YnlBhsfW2149mKXNdhNQXXwYTno` from location `https://www.bmoine.fr/`).

Knowing this, and while admitting that the webpage is using relative links to import its resources, there is sometimes the ability to play with the current location without changing the content that's delivered from the HTTP service, for exemple, using a dot-dot-slash attack (Directoy traversal). In fact I've found a frontend variant using the `path` segment of the URL that enables a Relative Path Overwrite attack, whereas traditional dot-dot-slash exploits `query` segment of the URL to perform a backend attack.

According to the [RFC-3986 - Uniform Resource Identifier (URI): Generic Syntax](https://tools.ietf.org/html/rfc3986)), this vulnerability is due to implementation inconsistency between browsers (all browsers are affected!) and HTTP servers (e.g. nginx, httpd):

```raw
   The path segments "." and "..", also known as dot-segments, are
   defined for relative reference within the path name hierarchy.  They
   are intended for use at the beginning of a relative-path reference
   (Section 4.2) to indicate relative position within the hierarchical
   tree of names.  This is similar to their role within some operating
   systems' file directory structures to indicate the current directory
   and parent directory, respectively.  However, unlike in a file
   system, these dot-segments are only interpreted within the URI path
   hierarchy and are removed as part of the resolution process (Section
   5.2).
```

```raw
   The pseudocode also refers to a "remove_dot_segments" routine for
   interpreting and removing the special "." and ".." complete path
   segments from a referenced path.  This is done after the path is
   extracted from a reference, whether or not the path was relative, in
   order to remove any invalid or extraneous dot-segments prior to
   forming the target URI.  Although there are many ways to accomplish
   this removal process, we describe a simple method using two string
   buffers.
```

In fact, the Web browser tries to remove dot-segments from the client side URL before trying to send an HTTP request to the server, but without processing urlentities where the server decodes them:

| Client-side URL               | Browser process                                 | Server-side URL               | HTTP server process                                                                                                                            |
|-------------------------------|-------------------------------------------------|-------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| `https://vps.bmoine.fr/../`   | Changes URL to `https://vps.bmoine.fr/`         | `https://vps.bmoine.fr/`      | Return default index from the `/` location (`https://vps.bmoine.fr/`)                                                                          |
| `https://vps.bmoine.fr/..%2F` | Keeps URL as-is (`https://vps.bmoine.fr/..%2F`) | `https://vps.bmoine.fr/..%2F` | Standardize location (remove dot-segments, but without redirection) then return default index from the `/` location (`https://vps.bmoine.fr/`) |

Further Reading:

 - [RPO](http://www.thespanner.co.uk/2014/03/21/rpo/)
 - [Relative Path Overwrite](http://support.detectify.com/customer/portal/articles/2088351-relative-path-overwrite)
 - [Path-Relative Stylesheet import (PRSSI)](http://blog.portswigger.net/2015/02/prssi.html)
 - [Directory traversal attack](https://en.wikipedia.org/wiki/Directory_traversal_attack)
 - [Subresource Integrity (SRI)](https://frederik-braun.com/using-subresource-integrity.html)
 - [A CDN that can not XSS you: Using Subresource Integrity](https://frederik-braun.com/using-subresource-integrity.html)
 - [W3C - Subresource Integrity](https://w3c.github.io/webappsec-subresource-integrity/)

## Video demo

[![Relative Path Overwrite Cross-Site Scripting demo](https://img.youtube.com/vi/gnQtOtku5_A/maxresdefault.jpg)](https://www.youtube.com/watch?v=gnQtOtku5_A "Relative Path Overwrite Cross-Site Scripting demo")

Please let me know if you need more information!
